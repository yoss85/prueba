import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IRemoteResponse } from '../interfaces/IRemoteResponse';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

  getArray(): Observable<IRemoteResponse> {
    return this.http.get<IRemoteResponse>('http://168.232.165.184/prueba/array');
  }

  getDict(): Observable<IRemoteResponse> {
    return this.http.get<IRemoteResponse>('http://168.232.165.184/prueba/dict');
  }
  
}

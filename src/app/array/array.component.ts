import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data-service.service';
import { Observable, of, throwError } from 'rxjs';
import { distinct, tap, retry, catchError } from 'rxjs/operators';
import { error } from 'util';
import { IRemoteResponse } from '../interfaces/IRemoteResponse';

@Component({
  selector: 'app-array',
  templateUrl: './array.component.html',
  styleUrls: ['./array.component.css']
})
export class ArrayComponent implements OnInit {

  // arrayData = [12, 19, 10, 3, 15, 16, 10, 8, 4, 9, 13, 15, 3, 14, 18, 20, 18, 1, 4, 1, 10, 2, 0, 8,
  //   5, 15, 18, 2, 0, 1, 3, 7, 16, 9, 10, 7, 17, 12, 3, 9, 13, 2, 18, 14, 3
  // ];
  fullArray: number[] = [];
  tableArray: number[] = [];
  sortedArray: number[] = [];
  data$: Observable<IRemoteResponse>;

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.data$ = this.dataService.getArray().pipe(
      tap(x => {
          this.fullArray = x.data;
          this.sortedArray = this.quickSort(x.data);
          for (const value of x.data) {
            if (!this.tableArray.includes(value)) {
              this.tableArray.push(value);
            }
          }
        }),
      );
  }

  countOccurrences(value: number): number {
    let result = 0;
    for (let item of this.fullArray) {

      if (value === item) {
        result++;
      }
    }

    return result;
  }

  /*************
   *  Sorting  *
   * ***********/

  partition(array: number[], left: number = 0, right: number = array.length - 1) {
    const pivot = array[Math.floor((right + left) / 2)];
    let i = left;
    let j = right;

    while (i <= j) {

      while (array[i] < pivot) {
        i++;
      }

      while (array[j] > pivot) {
        j--;
      }

      if (i <= j) {
        [array[i], array[j]] = [array[j], array[i]];
        i++;
        j--;
      }
    }

    return i;
  }

  quickSort(array: number[], left: number = 0, right: number = array.length - 1) {
    let index;

    if (array.length > 1) {
      index = this.partition(array, left, right);

      if (left < index - 1) {
        this.quickSort(array, left, index - 1);
      }

      if (index < right) {
        this.quickSort(array, index, right);
      }
    }

    return array;
  }

}

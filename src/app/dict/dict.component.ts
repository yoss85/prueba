import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataService } from '../services/data-service.service';
import { Subscription, Observable } from 'rxjs';
import { stringify } from 'querystring';
import { IRemoteResponse } from '../interfaces/IRemoteResponse';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-dict',
  templateUrl: './dict.component.html',
  styleUrls: ['./dict.component.css']
})
export class DictComponent implements OnInit {

  data: any[];
  chars: Array<string>;
  headers = [
    'a', 'b', 'c', 'd', 'e', 'f', 'g',
    'h', 'i', 'j', 'k', 'l', 'm', 'n',
    'o', 'p', 'q', 'r', 's', 't', 'u',
    'v', 'w', 'x', 'y', 'z', 'sum'];
    
  characterCounts: Array<Array<number>>;
  sumColumn: number[] = [];
  response$: Observable<IRemoteResponse>;

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.response$ = this.dataService.getDict().pipe(
      tap(x => {
        this.data = x.data;
        console.log('here here');
        this.characterCounts = [];
        const temp = 'abcdefghijklmnopqrstuvwxyz';
        this.chars = new Array(...temp);
        for (let i = 0; i < this.data.length; i++) {
          this.characterCounts[i] = this.countAllCharacters(this.data[i].paragraph);
          this.characterCounts[i].push(this.findAndSumNumbers(this.data[i].paragraph));
        }
      }),
    );
  }

  countAllCharacters(str: string): number[] {
    const result: number[] = [];
    for (let i = 0; i < this.chars.length; i++) {
      result[i] = this.countCharacters(this.chars[i], str);
    }
    return result;
  }

  countCharacters(character: string, str: string): number {
    const reg = new RegExp(character, 'gi');
    let num_matches = 0;
    const matchStr = str.match(reg);
    if (matchStr !== null) {
      num_matches = matchStr.length;
    }

    return num_matches;
  }

  findAndSumNumbers(str: string) {
    let strArray = str.split(' ');
    let numArray: number[] = [];
    strArray.forEach(x => x.replace(/^\D+/g, ''));
    strArray.forEach(x => {
      if (!isNaN(Number(x))) {
        numArray.push(Number(x));
      }
    });
    let result = 0;
    for (let i = 0; i < numArray.length; i++) {
      result += numArray[i];
    }
    console.log(result);
    return result;
  }

}
